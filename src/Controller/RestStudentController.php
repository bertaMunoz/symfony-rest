<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\Student;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormErrorIterator;

/**
 * @Route("/rest/student", name="rest_student")         
 */
//sera la base, le préfixe de toutes les routes du controller
class RestStudentController extends Controller
{
    private $serializer;
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/",methods={"GET"} )
     */
    //quand je ferai du get avec rest/student/ mènera sur cette route là  // c'est le préfixe // 
    //accessible uniquement par ce qui est posté en get => le http marche en get à la base. 
    // tout ce qui est mise à disposition de données devra être mis à dispo en get   => chasque fois que le client sera amené à récupérer des données, il faudra le mette en get => si le client nous envoi quelque chose, là ce sera en post. 
   // public function index()
    public function index(SerializerInterface $serializer)
    {
    //    //on crée une réponse http
    //    $response = new Response();
    //    //on indique que son contenu sera du json
    //    $response->headers->set("Content-Type", "application/json"); 
    //    //on indique que le code de retour est 200 ok 
    //    $response->setStatusCode(200);
    //    //on met comme contenu un tableau encodé en json
    //    $response->setContent(json_encode(["ga"=>"bu"])); //contenu du tableau encodé en json
       
       
    //     return $response;

    $student = new Student();
    $student->setName("test");
    $student->setsurname("surtest");
    $student->setLevel(5);
    $student->setTech(["symfony", "javascript"]);
    //le php ne sait pas transformer en json un new Student, donc, on va utiliser une méthode: soit on implémente dans l'entité la méthode serializable, puis mettre ds l'entité une public function serializable en donnant les détails(voir doc)//
    //symfony utilise une autre méthode: on installe en cli : composer require symfony/serializer  => qui met à disposition un package qui va serializer automatiquement. 

        $repo=$this->getDoctrine()->getRepository(Student::class);

    //repo doctrine sert à tout ce qui est requête sur la bdd , à faire tous les finds possibles
    //return new JsonResponse($student); // attend un objet et va essayer de le json encode //  donc va essayer de l'encoder 2 fois et ne va pas marcher car json par défaut quand on lui donne du new transforme en json et nous on lui donne déjà du  json; donc, on fait ça: 
    $json = $serializer->serialize($repo->findAll(), "json"); // on ultilise le serializer pour transformer un objet une entité php au format json  //// d'abbord un find all avec le repo, il ramène un tableau d'objets student qu'on donne à manger à la methode serialize de json pour qu'il les transforme en json. , son se retrouve avec ues string json qui représente tous les objets de ma bdd 
    return JsonResponse::fromJsonString($json); //on utilise la méthode statique fromJsoString de la classe jsonResponse pour créer une réponse http en json à partir de données déjà au format json // fromJsoString, si on lui donne quelque chose qui n'est pas du json, ça ne va pas marcher.  ///ce json on le donnee à manger à une réponse 
    }

    /**
     * @Route("/", methods={"POST"})
     */
    public function add(Request $request){

        $form = $this->createForm(StudentType::class);

        $form->submit(json_decode($request->getContent(), true)); // transforme le body de la request en tableau associatif et le donne à manger au submit. (true => transforme en tableau assoc)
             /// $body = $request->getContent();
        //on va faire une requête post qui ajoutera des données dans la base de données. 

        if($form->isSubmitted() && $form-> isValid()){
            $student = $form->getData();  // on récupère les données du formulaire
       // $student = $this->serializer->deserialize($body, Student::class, "json"); //
        $manager = $this->getDoctrine()->getManager(); //ici on fait appel au manager pour ajouter à la base de données
        $manager->persist($student);
        $manager->flush();
        //return new Response($student->getName() . $student->getSurname() . $student->getLevel()); // pour afficher ds clientRest
        return new JsonResponse(["id"=>$student->getId()]); // on envoie une réponse pour dire que l'id a bien été ajpouté à le bdd //voir imprimécran id // en post => il envoie ds la bdd et nous envoie l'id 10 // en get on vérifie qu'il a bien ajouté le truc imprimécran idid. 
        }
        //return new JsonResponse(["errors"=>"Yes"], 500); 
        return new JsonResponse(["errors"=>
                    $this->listFormErrors($form->getErrors())], 500); //cette méthode est déclarée plus bas

    }

    /**
     * @Route("/{student}", methods="GET")
     */
    public function single(Student $student) // nous permet de chopper un truc spécifique par son id
    {
        $json = $this->serializer->serialize($student, "json");
        return JsonResponse::fromJsonString($json);
    }

     /**
     * @Route("/{student}", methods="DELETE")
     */
    public function remove(Student $student) 
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($student);
        $manager->flush();
        
        return new Response();
    }
    /**
     * @Route("/{student}", methods="PUT")
     */
    public function update(Student $student, Request $request) 
    {
        $body = $request->getContent();
        $updated = $this->serializer->deserialize($body, Student::class, "json"); //

        $manager = $this->getDoctrine()->getManager();
        $student->setName($updated->getName());
        $student->setSurname($updated->getSurname());

        $student->setLevel($updated->getLevel());
        $student->setTech($updated->getTech());

       
        $manager->flush();
        
        return new Response("", 204);
    }

    private function listFormErrors(FormErrorIterator $errors){ //renvoie une liste d'erreurs
//convertit le FormErrorIterator en un tableau de strings. 
        foreach ($errors as $error) {
            $list[] = $error->getMessage();
        }
    }
}

